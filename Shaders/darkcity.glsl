uniform float x;
uniform float y;
uniform float radius;

float lerp(float a, float b, float f) {
    return a + f * (b - a);
}

float quad(float a, float b, float f) {
	return a + pow(f, 2)*(b-a);
}

vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords) {
    vec4 texturecolor = Texel(texture, texture_coords);

    vec4 pos = gl_FragCoord;
    float tx = pos[0];
    float ty = pos[1];

    float dist = sqrt(pow(tx-x, 2) + pow(ty-y, 2));
    float grey = texturecolor[0]*0.4
    		   + texturecolor[1]*0.6 +
        	   + texturecolor[2]*0.2;
    if (dist <= radius) {
    	texturecolor = vec4(quad(grey, texturecolor[0], 1-dist/radius),
    						quad(grey, texturecolor[1], 1-dist/radius),
							quad(grey, texturecolor[2], 1-dist/radius),
							quad(0, texturecolor[3], 1-dist/radius));
    }else {
    	texturecolor= vec4(grey, grey, grey, 0);
    }

    return texturecolor * color;
}
