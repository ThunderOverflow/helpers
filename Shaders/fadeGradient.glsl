uniform float startDark = 200.;

float lerp(float a, float b, float f) {
    return a + f * (b - a);
}

vec4 effect(vec4 color, sampler2D texture, vec2 tc, vec2 sc) {
	vec4 t = Texel(texture, tc);
	float f = clamp(sc.y/startDark, 0., 1.);
	
	return vec4(t.rgb*(f*f), t.a)*color;
}