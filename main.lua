local Object3D = require "3D Renderer.Object3D"
local Renderer = require "3D Renderer.Renderer"
local cpml = require "3D Renderer.cpml.init"
local vec3 = cpml.vec3
local loader = require "3D Renderer.obj_loader"

local ObjectA = Object3D:new2D(vec3(0, 0, -100), vec3(0, 0, 0), 100, 100, love.graphics.newImage("Tests/background.png"))
local ObjectB = Object3D:new(vec3(0, 0, 0), vec3(0, 0, 0), nil,
	love.graphics.newImage("Tests/4096_earth.jpg"), 
	loader.load("Tests/earth.obj"))
local camera  = Object3D:new(
	vec3.new(0, 0, 0), 
	vec3.new(0, 0, 0)
)

ObjectB:setNormalTexture(love.graphics.newImage("Tests/4096_normal.jpg"))











love.mouse.setRelativeMode(true)










function love.draw()
	Renderer:prepare()
	Renderer:draw(ObjectA)
	Renderer:draw(ObjectB)
	
	local pos = Renderer.camera:getPosition()
	love.graphics.print(("%.2f %.2f %.2f"):format(pos.x, pos.y, pos.z))
	
	local rot = Renderer.camera:getRotation()
	--love.graphics.print(("%.2f %.2f %.2f"):format(rot.x, rot.y, rot.z), 0, 16)
	
	love.graphics.draw(Renderer.shadow_canvas, 600, 0, 0, 0.25, 0.25)
	love.graphics.draw(Renderer.canvas, 0, 0, 0)
end

function love.update(dt)
	local sin = math.sin
	local cos = math.cos
	local speed = 100
	local cameraPos = camera:getPosition()
	local cameraRot = camera:getRotation()
	local w, h = love.window.getMode()
	
	
	if love.keyboard.isDown("a") then
		cameraPos.x = cameraPos.x + speed*cos(-cameraRot.y)*dt
		cameraPos.z = cameraPos.z + speed*sin(cameraRot.y)*dt
	elseif love.keyboard.isDown("d") then
		cameraPos.x = cameraPos.x - speed*cos(-cameraRot.y)*dt
		cameraPos.z = cameraPos.z - speed*sin(cameraRot.y)*dt
	end
	
	
	if love.keyboard.isDown("w") then
		cameraPos.x = cameraPos.x + speed*sin(-cameraRot.y)*dt
		cameraPos.z = cameraPos.z + speed*cos(cameraRot.y)*dt
	elseif love.keyboard.isDown("s") then
		cameraPos.x = cameraPos.x - speed*sin(-cameraRot.y)*dt
		cameraPos.z = cameraPos.z - speed*cos(cameraRot.y)*dt
	end
	
	cameraRot.x =  -(love.mouse.getY()-h/2)/h * math.pi
	
	camera:setPosition(cameraPos):setRotation(cameraRot)
	
	Renderer:setCameraPositionFromVec3(cameraPos)
	Renderer:setCameraRotationFromVec3(cameraRot)
end

function love.mousemoved(x, y, dx, dy)
	local speed = 0.01
	local cameraRot = camera:getRotation()

	cameraRot.y = cameraRot.y + dx *speed/1
	
	if cameraRot.y > math.pi*2 then
		cameraRot.y = math.pi*2 - cameraRot.y
	end
	
	if cameraRot.y < -math.pi*2 then
		cameraRot.y = -math.pi*2 - cameraRot.y
	end
	
	camera:setRotation(cameraRot)
end