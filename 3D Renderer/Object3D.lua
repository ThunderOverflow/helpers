local cpml = require "3D Renderer.cpml.init"
local vec3 = cpml.vec3
local mat4 = cpml.mat4

local Object3D = {}
Object3D.customformat = {
	{"VertexPosition", "float",	3},
	{"VertexTexCoord", "float", 2},
	{"Normal", "float", 3}
}
Object3D.position = vec3.new()
Object3D.rotation = vec3.new()
Object3D.applyGlobalLight = true
Object3D.bumpTexture = nil
Object3D.normalTexture = nil


function Object3D:new(position, rotation, vertices, texture, object)
	local o = {}
	o.__index = self
	setmetatable(o, o)
	o.position = position
	o.rotation = rotation
	
	if object then
		local final = {} 
		
		--O(MAX)
		
		
		for i, v in ipairs(object.f) do
			for j, w in ipairs(v) do
				for k = 2,#v-2  do
					table.insert(final, {
						object.v[v[1].v].x, 
						object.v[v[1].v].y,
						object.v[v[1].v].z,
						object.vt[v[1].vt].u,
						object.vt[v[1].vt].v,
						object.vn[v[1].vn].x,
						object.vn[v[1].vn].y,
						object.vn[v[1].vn].z
					})
					for vr = k, k+1 do 
						table.insert(final, {
							object.v[v[vr].v].x, 
							object.v[v[vr].v].y,
							object.v[v[vr].v].z,
							object.vt[v[vr].vt].u,
							object.vt[v[vr].vt].v,
							object.vn[v[vr].vn].x,
							object.vn[v[vr].vn].y,
							object.vn[v[vr].vn].z
						})
					end
					
					
				end 
			end 
		end
		

		o.mesh = love.graphics.newMesh(self.customformat, final, "triangles")
		if texture then o.mesh:setTexture(texture) end
	elseif vertices then
		o.mesh = love.graphics.newMesh(self.customformat, vertices, "triangles")
		if texture then o.mesh:setTexture(texture) end
	end
	
	return o
end

function Object3D:new2D(position, rotation, width, height, texture)
	local vertices = {
		{0, 	0, 		0, 	1, 	1, 0, 0, 0},
		{width, 0, 		0,	0, 	1, 0, 0, 0},
		{0, height, 	0,	1, 	0, 0, 0, 0},
		{width, 0, 		0,	0,	1, 0, 0, 0},
		{0, height, 	0,	1,	0, 0, 0, 0},
		{width, height,	0,	0,	0, 0, 0, 0},
	}
			
	return self:new(position, rotation, vertices, texture)
end

function Object3D:draw()
	if self.mesh then
		love.graphics.draw(self.mesh)
	end
end

function Object3D:getCamModel()
	local matrix = mat4:new()
		matrix:rotate(matrix, self.rotation.x, vec3.new(1, 0, 0))
		matrix:rotate(matrix, self.rotation.y, vec3.new(0, 1, 0))
		matrix:rotate(matrix, self.rotation.z, vec3.new(0, 0, 1))
		matrix:translate(matrix, self.position)
	
	return matrix
end

function Object3D:getModel()
	local matrix = mat4:new()
		matrix:translate(matrix, self.position)
		matrix:rotate(matrix, self.rotation.x, vec3.new(1, 0, 0))
		matrix:rotate(matrix, self.rotation.y, vec3.new(0, 1, 0))
		matrix:rotate(matrix, self.rotation.z, vec3.new(0, 0, 1))
		
	return matrix
end

function Object3D:translate(vector)
	self.position = self.position + vector
end

function Object3D:rotate(vector)
	self.rotation = vec3.rotate(self.rotation, vector.x, vec3.new(1, 0, 0))
	self.rotation = vec3.rotate(self.rotation, vector.y, vec3.new(0, 1, 0))
	self.rotation = vec3.rotate(self.rotation, vector.z, vec3.new(0, 0, 1))
end

function Object3D:setPosition(vector)
	self.position = vector
	return self
end

function Object3D:setRotation(vector)
	self.rotation = vector
	return self
end

function Object3D:setTexture(texture)
	if self.mesh then self.mesh:setTexture(texture) end
	return self
end

function Object3D:setGlobalLight(apply)
	self.applyGlobalLight = apply
	return self
end

function Object3D:setBumpTexture(texture)
	self.bumpTexture = texture
	return self
end

function Object3D:setNormalTexture(texture)
	self.normalTexture = texture
	return self
end


function Object3D:getPosition()
	return self.position
end

function Object3D:getRotation()
	return self.rotation
end

function Object3D:receiveGlobalLight()
	return self.applyGlobalLight
end

function Object3D:getBumpTexture()
	return self.bumpTexture
end

function Object3D:getNormalTexture()
	return self.normalTexture
end


return Object3D