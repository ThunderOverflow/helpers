local cpml = require "3D Renderer.cpml.init"
local vec2 = cpml.vec2
local vec3 = cpml.vec3
local mat4 = cpml.mat4

local Object3D = require "3D Renderer.Object3D"

local Renderer = {}
Renderer.SHADER = love.graphics.newShader("3D Renderer/Shaders/shader.glsl")
Renderer.SHADOW_SHADER = love.graphics.newShader("3D Renderer/Shaders/shadow_shader.glsl")

Renderer.camera = Object3D:new(vec3(0, 0, 0), vec3(0, 0, 0))
Renderer.light = Object3D:new(vec3(0, 0, 500), vec3(math.pi, 0, 0))
Renderer.projection = mat4.from_perspective(90, 800/600, 0.1, 10000)

Renderer.shadow_canvas = love.graphics.newCanvas(800, 600)
Renderer.shadow_depth  = love.graphics.newCanvas(800, 600, {format = "depth16", readable = true})
Renderer.canvas = love.graphics.newCanvas(800, 600)

Renderer.shadow_depth:setDepthSampleMode("less")

function Renderer:prepare()
	love.graphics.push("all")
	love.graphics.setCanvas({self.canvas, depth = true})
	love.graphics.clear()
	love.graphics.pop()
end



function Renderer:drawShadow(Object)
	love.graphics.push("all")
	love.graphics.setShader(self.SHADOW_SHADER)
	love.graphics.setDepthMode("less", true)
	love.graphics.setCanvas({self.shadow_canvas, depthstencil = self.shadow_depth})
	love.graphics.clear()

	self.SHADOW_SHADER:send("projection", "column", self.projection)
	self.SHADOW_SHADER:send("view", "column", self.light:getCamModel())
	self.SHADOW_SHADER:send("model", "column", Object:getModel())	
	
	Object:draw()
	love.graphics.pop()
end


function Renderer:draw(Object)
	self:drawShadow(Object)
	
	
	love.graphics.push("all")
	love.graphics.setShader(self.SHADER)
	love.graphics.setDepthMode("less", true)
	love.graphics.setCanvas({self.canvas, depth = true})
	
	local cpos = self.camera:getPosition()
	self.SHADER:send("projection", "column", self.projection)
	self.SHADER:send("view", "column", self.camera:getCamModel())
	self.SHADER:send("normal_map_light_position", {-cpos.x, cpos.y, -cpos.z})
	self.SHADER:send("model", "column", Object:getModel())	
	self.SHADER:send("apply_diffuse_light", Object:receiveGlobalLight() and 1 or 0)
	self.SHADER:send("depthcanvas", self.shadow_canvas)
	
	local bump = Object:getBumpTexture()
	local normal = Object:getNormalTexture()
	
	if normal then self.SHADER:send("normal_texture", normal) end
	
	Object:draw()
	love.graphics.pop()
end

function Renderer:setCameraPositionFromVec3(position)
	self.camera:setPosition(position)
end

function Renderer:setCameraRotationFromVec3(rotation)
	self.camera:setRotation(rotation)
end


return Renderer