#pragma language glsl3

#ifdef VERTEX
// Scene transformations
uniform mat4 projection; // Projection, view, model transform
uniform mat4 view;  // View, model transform
uniform mat4 model;
uniform mat4 vertex_position;

uniform mat4 light_view;
uniform mat4 light_projection;

// Original model data
attribute vec3 a_Vertex;
attribute vec3 a_Color;
attribute vec3 Normal;

// Data (to be interpolated) that is passed on to the fragment shader
varying vec3 v_Vertex;
varying vec4 v_Color;
varying vec3 v_Normal;
varying mat4 v_View;

varying vec4 v_Vertex_relative_to_light;


vec4 position(mat4 transform_projection, vec4 vertex_position) {
	// Perform the model and view transformations on the vertex and pass this
	// location to the fragment shader.
	v_Vertex = vec3((view * model *  vertex_position).xyz );
	
	// Perform the model and view transformations on the vertex's normal vector
	// and pass this normal vector to the fragment shader.
	v_Normal = vec3((view * model * vec4(Normal, 0)).xyz );
	
	// Pass the vertex's color to the fragment shader.
	v_Color = vec4(1.0);
	
	//Pass model to the fragment shader.
	v_View = view;
	
	v_Vertex_relative_to_light = light_projection * light_view * model * vertex_position;
	
	return projection * view * model * vertex_position;
}
#endif




#ifdef PIXEL
#pragma language glsl3
// Light model
uniform vec3 diffuse_light_position = vec3(0., 0., 0.);
uniform vec3 normal_map_light_position = vec3(0.);

// Data coming from the vertex shader
varying vec3 v_Vertex;
varying vec4 v_Color;
varying vec3 v_Normal;
varying mat4 v_View;

//Data for effects
uniform sampler2D normal_texture;
uniform sampler2D shadow_map;

//Effects switchs
uniform int apply_diffuse_light;
uniform int apply_normal_mapping;

uniform sampler2DShadow depthcanvas;

//light position 
//vertex position
//normal vector
float diffuse(vec3 lightPosition, vec3 vertex, vec3 normal) {
	
	vec3 to_light;
	vec3 vertex_normal;
	float cos_angle;
	
	// Calculate a vector from the fragment location to the light source
	to_light = lightPosition - vertex;
	to_light = normalize( to_light );
	
	// The vertex's normal vector is being interpolated across the primitive
	// which can make it un-normalized. So normalize the vertex's normal vector.
	vertex_normal = normalize( normal );
	
	// Calculate the cosine of the angle between the vertex's normal vector
	// and the vector going to the light.
	cos_angle = dot(vertex_normal, to_light);
	
	
	return clamp(cos_angle, 0.0, 1.0);
}

vec4 effect(vec4 color, Image tex, vec2 texture_coords, vec2 screen_coords) {
	vec4 texturecolor = Texel(tex, texture_coords);

	if (apply_diffuse_light == 0) {discard;} 
	if (texture(depthcanvas, vec2(0., 0.), 0.) > 0.) {discard;}
    
	vec3 diffuseLightPosition = (v_View*vec4(diffuse_light_position, 0.0)).xyz;
	vec3 normalMapLightPosition = (v_View*vec4(normal_map_light_position, 0.0)).xyz;
	
	
	//Color from diffuse lightning
	float diffuse_value = diffuse(diffuseLightPosition, v_Vertex, v_Normal);
    vec4 diffuse_color = vec4(vec3(v_Color) * diffuse_value, v_Color.a);
    
    //Color from normal mapping
    vec4 normal_frag = Texel(normal_texture, texture_coords);
	vec3 normal_tex = ((normal_frag*2.)-1.).xyz;
	float normal = diffuse(normalMapLightPosition, v_Vertex, normal_tex);	
	vec4 normal_color = vec4(vec3(v_Color) * normal, v_Color.a);
	
	return texturecolor * color * normal_color;
}
#endif