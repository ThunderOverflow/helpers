

#ifdef VERTEX
// Scene transformations
uniform mat4 projection; // Projection, view, model transform
uniform mat4 view;  // View, model transform
uniform mat4 model;

varying vec4 v_pos;

vec4 position(mat4 transform_projection, vec4 vertex_position) {
	v_pos = projection * view * model * vertex_position;
	return projection * view * model * vertex_position;
}
#endif


#ifdef PIXEL
varying vec4 v_pos;

vec4 effect(vec4 color, Image tex, vec2 texture_coords, vec2 screen_coords) {
	return vec4(v_pos.z);
}
#endif